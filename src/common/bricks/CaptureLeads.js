import React from 'react'
import styled from 'styled-components'
import { Container, Flex } from '../styles'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    font-size: 28px;
    line-height: 40px;
    padding: 50px 0 60px;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        padding: 30px 0;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 24px;
        padding: 20px 0;
    }
`

const LocFlex = styled(Flex)`
    justify-content: center;

    div{
        width: 100%;
    }

    img{
        max-width: 805px;
        width: calc(100% - 30px);
        margin: 0 15px;
    }

    @media(max-width: 1198px){
        flex-direction: column;

        img{
            display: block;
            margin: 15px auto;
        }
    }
`

const CaptureLeads = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <Text>{props.acf.text}</Text>
                <LocFlex>
                    {
                        props.acf.repeater.map(el =>
                            <div>
                                <img alt={el.img_alt} src={el.img} />
                            </div>
                        )
                    }
                </LocFlex>
            </Container>
        </Article>
    )
}

export default CaptureLeads