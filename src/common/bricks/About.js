import React from 'react'
import styled from 'styled-components'
import { Container, Link } from '../styles'
import MiniForm from './../mini-form/Form'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    text-align: center;
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`



const List = styled.ul`
    li:nth-child(odd){
        flex-direction: row;
        @media(max-width: 1198px){
            flex-direction: column
        }
    }
`

const ListItem = styled.li`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row-reverse;
    margin-top: 165px;
    @media(max-width: 1198px){
        flex-direction: column;
        margin-top: 80px;
    }

`

const Img = styled.img`
    max-width: 800px;
    min-width: 500px;
    width: 50vw;
    @media(max-width: 764px){
        min-width: 290px;
        width: 80vw;
    }
`

const ImgWrapper = styled.div`
    padding: 0 15px;
    @media(max-width: 764px){
        padding: 0;
    }
`

const TextWrapper = styled.div`
    width: 665px;
    padding: 0 15px;
    @media(max-width: 764px){
        width: 100%;
        padding: 0;
    }
`

const ListTitle = styled.h3`
    font-size: 18px;
    font-weight: bold;
    color: #377DFF;
    text-transform: uppercase;
`

const ListSubTitle = styled.h4`
    padding: 26px 0 14px;
    font-size: 22px;
    font-weight: bold;
    @media(max-width: 764px){
        font-size: 18px;
    }
`

const ListText = styled.p`
    padding-bottom: 50px;
    color: #6E7276;
    line-height: 28px;
    @media(max-width: 1198px){
        font-size: 14px;
    }
`

const ListButton = styled(Link)`
    transition: all.2s linear;

&:hover {
    border-color: #377DFF;
    background-color: #FFFFFF;
    color: #000000;
}
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 28px;
    }
`

const MiniFormWrap = styled.div`
margin-top: 240px;

@media (max-width: 1198px) {
    margin-top: 100px;
}
`

const About = (props) => {
    const array = Object.values(props.acf.about)
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <List>
                    {
                        array.map(el =>
                            <ListItem>
                                <ImgWrapper>
                                    <Img alt={el.img_alt} src={el.img0} />
                                </ImgWrapper>
                                <TextWrapper>
                                    <ListTitle>{el.title}</ListTitle>
                                    <ListSubTitle>{el.text_title}</ListSubTitle>
                                    <ListText>{el.text}</ListText>
                                    {
                                        el.button_type
                                            ? <ListButton to={el.button_link}>{el.button_text}</ListButton>
                                            : <ListButton rel="noreferrer" target='_blank' as='a' href={el.button_link}>{el.button_text}</ListButton>
                                    }
                                </TextWrapper>
                            </ListItem>
                        )
                    }

                </List>

                {
                    props.acf.form
                        ? <MiniFormWrap>
                            <MiniForm acf={props.acf.form} />
                        </MiniFormWrap>
                        : null
                }


            </Container>
        </Article>
    )
}

export default About