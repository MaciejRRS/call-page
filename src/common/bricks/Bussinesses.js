import React from 'react'
import styled from 'styled-components'
import { Container } from '../styles'
import Background from '../../sprites/bussinesses_bg.png'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    max-width: 1080px;
    margin: 0 auto 100px;
    @media(max-width: 1198px){
        margin-bottom: 75px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 50px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 60px;
    max-width: 1370px;
    margin: 0 auto;
    @media(max-width: 1198px){
        max-width: 768px;
        grid-row-gap: 35px;
    }

    @media(max-width: 768px){
        grid-template-columns: 1fr;
        grid-row-gap: 30px;
    }
`

const BlockWrap = styled.div`
    width: 385px;
    height: 400px;
    border-radius: 50%;
    background: #FFFFFF;
    box-shadow: 0 3px 6px #00000016;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    align-self: center;
    justify-self: center;

    @media(max-width: 1300px) {
        width: 350px;
        height: 360px;
    }

    @media(max-width: 1198px) {
        width: 199px;
        height: 196px;
    }
`



const TitleBlock = styled.h3`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    color: #3F4143;
    margin-bottom: 25px;
    text-transform: uppercase;
    font-weight: bold;
    text-align: center;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 28px;
        margin-bottom: 20px;
        line-height: 40px;
    }

    @media (max-width: 768px) {
        color: #FF5971;
    }
`

const TextBlock = styled.p`
    color:#6E7276;
    font-size: 28px;
    line-height: 40px;
    text-align: center;
    padding: 0 30px;

    @media(max-width: 1198px) {
        font-size: 14px;
        line-height: 22px;
    }
`

const BottomBG = styled.div`
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 100%;
    width: 100%;
    height: 305px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);

    @media(max-width: 1198px) {
    height: 160px;
    }

    @media(max-width: 768px) {
    background-size: 0;
    }
`

const Wrapper = styled.div`
    position: relative;
    width: 100%;
`

const Bussinesses = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
            </Container>
            <Wrapper>
                <Container>
                    <Grid>
                        {props.acf.repeater_bussinesses.map(el =>
                            <BlockWrap>
                                <TitleBlock>{el.tilte_repeater}</TitleBlock>
                                <TextBlock>{el.text_repeater}</TextBlock>
                            </BlockWrap>
                        )}
                    </Grid>
                </Container>
                <BottomBG></BottomBG>
            </Wrapper>



        </Article>
    )
}

export default Bussinesses