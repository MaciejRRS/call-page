import React from 'react'
import { Container, Text } from '../styles'
import styled from 'styled-components'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
    @media(max-width: 900px){
        flex-direction: column;
        margin: -30px 0;
    }
`

const Item = styled.div`
    width: 370px;   
    text-align: center;
    padding: 0 15px;

    img{
        display: block;
        margin: 0 auto;
        max-width: 250px; 
        height: 200px;
    }

    @media(max-width: 1198px){
        img{
            max-width: 180px;
            max-height: 150px;
            height: 100%;
        }
    }

    @media(max-width: 900px){
        margin: 30px auto;
        width: 100%;
        padding: 0;
        img{
            max-width: 250px;
            max-height: 100%;
            height: auto;
            width: 100%;
        }
    }
`

const Number = styled.h3`
    padding: 40px 0 10px;
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding: 20px 0 5px;
    }
    @media(max-width: 764px){
    }
`

const Numbers = (props) => {
    return (
        <Article> 
            <Container>
                <Flex>
                    {
                        props.acf.repeater.map(el =>
                            <Item>
                                <img  alt={el.img_alt}  src={el.img} />
                                <Number>{el.number}</Number>
                                <Text>{el.text}</Text>
                            </Item>
                        )
                    }
                </Flex>
            </Container>
        </Article>
    )
}

export default Numbers