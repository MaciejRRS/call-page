import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Container, Text, Flex } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    max-width: 1082px;
    margin: 0 auto 100px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 60px;
    }
    @media(max-width: 764px){  
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 40px;
    }
`

const TextTitle = styled.h3`
    font-size: 28px;
    line-height: 40px;
    font-weight: bold;
    max-width: 420px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        padding-top: 60px;
    }
    @media(max-width: 764px){  
        font-size: 18px;
        line-height: 28px;
        padding-top: 45px;
    }
`

const TextPart = styled.div`
    max-width: 665px;
    padding-right: 30px;
    @media(max-width: 1198px){
        padding-right: 0;
    }
`

const ImgPart = styled.div`
    max-width: 526px;
    img{
        width: 100%;
    }
    @media(max-width: 1198px){
        max-width: 665px;
        width: 100%;
    }
`

const LocText = styled(Text)`
    line-height: 24px;
    padding: 30px 0 50px;
    @media(max-width: 764px){  
        font-size: 14px;
        line-height: 20px;
    }
    @media(max-width: 480px){
        padding: 20px 0 30px;
    }
`

const LocFlex = styled(Flex)`
    @media(max-width: 1198px){
        flex-direction: column-reverse;
    }
`

const Link = styled(NavLink)`
    border: 2px solid #377DFF;
    color: #000;
    font-weight: bold;
    padding: 20px 50px;
    border-radius: 6px;
    box-shadow: 0px 3px 6px 0px #D7E5FF;
    transition: .2s linear;

    &:hover {
        background-color: #377DFF;
        color: #fff;
    }

    @media(max-width: 764px){  
        font-size: 16px;
    }

    @media(max-width: 480px){
        display: block;
        width: 100%;
        box-sizing: border-box;
        text-align: center;
        padding: 10px 0;
    }
`

const LocContainer = styled(Container)`
    max-width: 1360px;
`

const TopReasons = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <LocContainer>
                <Title>{props.acf.title}</Title>
                <LocFlex>
                    <TextPart>
                        <TextTitle>{props.acf.text_title}</TextTitle>
                        <LocText>{props.acf.text}</LocText>
                        {props.acf.button_type
                            ? <Link to={props.acf.button_link}>{props.acf.button_text}</Link>
                            : <Link as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</Link>
                        }

                    </TextPart>
                    <ImgPart>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </ImgPart>
                </LocFlex>
            </LocContainer>
        </Article>
    )
}

export default TopReasons