import React from 'react'
import styled from 'styled-components'
import { Container, Flex } from '../styles'
import Background from '../../sprites/conversationRate.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 100px;
    bottom: 0;
    left: 0;
    z-index: 0;
    @media(max-width: 1760px){
        display: none;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin: 0 0 50px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin: 0 0 40px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin: 0 0 25px;
    }
`

const SubTitle = styled.h3`
    font-size: 45px;
    line-height: 60px;
    text-align: center;
    font-weight: normal;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Text = styled.p`
    padding: 60px 0;
    font-size: 28px;
    line-height: 45px;
    text-align: center;
    color: #6E7276;
    max-width: 700px;
    margin: 0 auto;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        padding: 45px 0;
    }
    @media(max-width: 764px){
        padding: 30px 0;
        font-size: 18px;
        line-height: 28px;
    }
`

const Item = styled.div`
    max-width: 526px;
    width: 100%;
    min-height: 150px;
    background-color: #fff;
    border-radius: 12px;
    margin-bottom: 30px;
    box-sizing: border-box;
    padding: 30px;
    box-shadow: 0 3px 6px 0 #F3F4F6;
    @media(max-width: 1760px){
        margin: 0 auto;
    }
`

const ItemText = styled.p`
    color: #6E7276;
    line-height: 24px;
    font-size: 16px;
    padding-left: 60px;
    position: relative;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }

    &::before{
        content: '✓';
        font-size: 60px;
        line-height: 80px;
        color: #FF5971;
        font-weight: bold;
        position: absolute;
        left: -15px;
        top: -30px;
    }
`

const LocFlex = styled(Flex)`
    @media(max-width: 1760px){
        display: grid;
        grid-template-columns: 1fr;
        grid-row-gap: 30px;
    }
`

const ConversationRate = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <SubTitle>{props.acf.sub_title}</SubTitle>
                <Text>{props.acf.text}</Text>
                <LocFlex>
                    {
                        props.acf.repeater.map(el =>
                            <Item>
                                <ItemText>{el.text}</ItemText>
                            </Item>
                        )
                    }
                </LocFlex>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}

export default ConversationRate