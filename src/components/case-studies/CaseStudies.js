import React, { useState, useEffect } from 'react'
import Hero from './components/Hero'
import { connect } from "react-redux";
import Content from './components/Content';
import SEO from './../../common/SEO/seo'

const CaseStudies = (props) => {

    const [productsPrototype, productsPrototypeChange] = useState([...props.caseStudies])
    const [products, productsChange] = useState([...productsPrototype]);

    useEffect(() => {
        productsPrototypeChange([...props.caseStudies])
        productsChange([...props.caseStudies])
    }, [props])

    let filter = (value) => {
        productsChange(productsPrototype.filter(el => el.acf.type === value))
    }

    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} filter={filter} />
            <Content posts={products} totalPosts={props.totalCaseStudies} isFetchingAddData={props.isFetchingAddData} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[22].acf, // Case Studies
        caseStudies: state.wpAcfReducer.caseStudies,
        totalCaseStudies: state.wpAcfReducer.totalCaseStudies,
        isFetchingAddData: state.wpAcfReducer.isFetchingAddData
    }
}

export default connect(mapStateToProps, {})(CaseStudies)