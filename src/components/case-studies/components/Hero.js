import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    width: calc(100% - 120px);
    max-width: 1360px;
    margin: 0 auto;
    padding-bottom: 120px;
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    padding: 60px 0 50px;
    text-align: center;

    @media (min-width: 992px){
        font-size: 45px;
        line-height: 60px;
    }

    @media (max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    font-size: 28px;
    line-height: 40px;
    text-align: center;
    color: #6E7276;

    @media (min-width: 992px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Flex = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    margin-top: 60px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr 1fr 1fr;
    }
    @media(max-width: 940px){  
        grid-template-columns: 1fr 1fr 1fr;
    }   
    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 564px){
        grid-column-gap: 15px;
        grid-row-gap: 15px;
    }
    @media(max-width: 440px){
        grid-template-columns: 1fr;
    }
`

const Item = styled.div`
    width: 100%;
    text-align: center;
    padding: 15px 15px 30px;
    box-sizing: border-box;
    color: #000;
    transition: .2s linear;
    box-shadow: 0 3px 6px #00000016;

    &:hover{
        color: #377DFF;
        transform: translate(2px, -4px);
    }

    img{
        margin-top: 30px;
        margin-bottom: 20px;
        max-width: 90px;
        height: auto;
    }

    @media(max-width: 992px){
        font-size: 16px;
        line-height: 24px;

        img{
            max-width: 60px;
            height: auto;
        }
    }
    
`

const Hero = (props) => {
    return (
        <article>
            <Container>
                <Title>{props.acf.title}</Title>
                <Text>{props.acf.text}</Text>
                <Flex>
                    {props.acf.repeater.map(el =>
                        <Item onClick={() => {props.filter(el.filter_type)}}>
                            <img alt={el.icon_alt} src={el.icon}/>
                            <p>{el.text}</p>
                        </Item>
                    )}
                </Flex>
            </Container>
        </article>
    )
}

export default Hero