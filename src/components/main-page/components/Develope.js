import React from 'react'
import styled from 'styled-components'
import { Container } from '../../../common/styles'

const Article = styled.article`
    position: relative;
    z-index: 10;
    padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
        margin-bottom: 260px;
    }

`

const BgArticle = styled.div`
    position: relative;
    z-index: 10;
    padding: 20px 0;
    background-color: #377DFF;
    @media(max-width: 1199px){
        background-color: transparent;
    }

`

const TextWrapper = styled.div`
    width: 655px;
    padding: 45px 0;
    margin-left: 30px;
     position: relative;
    color: #FFFFFF;
    box-sizing: border-box;
    padding: 45px 90px;
    p {
        font-size: 18px;
        line-height: 24px;
    }

    p.title{
        padding-top: 22px;
        font-size: 22px;
        line-height: 30px;
        font-weight: bold;
        color: #FFF;

    }

    &::before{
        position: absolute;
        content: '„';
        top: 30px;
        left: 40px;
        font-size: 60px;
        font-weight: bold;
        color: #ffffff;
    }

    &::after{
        position: absolute;
        content: '„';
        bottom: 60px;
        right: 90px;
        font-size: 60px;
        font-weight: bold;
        color: #ffffff;
    }


    @media(max-width: 1198px){  
        width: 500px;
        background-color: #377DFF;
        box-sizing: border-box;
        padding: 45px 90px 80px;
        display: flex;
        flex-direction: column;
        box-shadow: 0 3px 6px 0 #00000016;


        &::before{
            top: 0px;
            left: 40px;
        }

        &::after{
            bottom: 50px;
            right: 40px;

            @media(max-width:400px) {
                right: 15px;
            }
        }

        p{
            font-size: 16px;
            line-height: 24px;
        }
    }

    @media(max-width: 680px){  
        width: 100%;
        margin: 0;

        p{
            font-size: 14px;
            line-height: 20px;
        }
    }

    @media(max-width: 460px){  
        padding: 45px 30px 80px;
        &::before{
            top: -10px;
            left: 20px;
        }
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-evenly;
    flex-direction: row;
    align-items: center;
    position: relative;
    flex-direction: row-reverse;
    z-index: 10;
    @media(max-width: 1198px){  
        flex-direction: row;
        justify-content: flex-start;
    }
`

const Img = styled.img`
    @media(max-width: 1198px){  
        position: absolute;
        transform: translate(125%, 65%);
        max-width: 300px;
    }
    @media(max-width: 764px){  
        max-width: 240px;
        bottom: 0;
        right: 0;
        left: 50%;
        transform: translate(-50%, 60%);
    }
    /* @media(max-width: 460px){  
        bottom: 0;
        right: 0;
        left: 50%;
        transform: translate(-50%, 80%);
    } */
`

const Title = styled.h2`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    text-align: center;
    font-weight: bold;
    margin-bottom: 160px;

@media(max-width: 1199px) {
    max-width: 510px;
    font-size: 45px;
    line-height: 60px;
    margin: 0 auto 80px;
}

@media(max-width: 767px) {
    max-width: 100%;
    font-size: 28px;
    line-height: 40px;
    margin-bottom: 55px;
}
`

const Develope = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
            </Container>
            <BgArticle>
                <Container>
                    <Flex>
                        <TextWrapper>
                            <p>{props.acf.quote}</p>
                            <p className='title'>{props.acf.quote_author}</p>
                        </TextWrapper>
                        <Img alt={props.acf.img_alt} src={props.acf.img} />
                    </Flex>
                </Container>
            </BgArticle>

        </Article>
    )
}
export default Develope