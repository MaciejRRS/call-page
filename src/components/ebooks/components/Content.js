import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Container, Link } from '../../../common/styles'

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 60px;
    grid-row-gap: 60px;
    max-width: 984px;
    margin: 0 auto;
    @media (max-width: 1096px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 764px){
        grid-template-columns: 1fr;
    }
`

const Item = styled.div`
position: relative;
`

const Img = styled.img`
    width: 100%;
`

const Title = styled.h2`
    padding: 40px 0 20px;
    font-size: 28px;
    line-height: 40px;
    color: #000;
    @media (max-width: 1096px){
        font-size: 22px;
        line-height: 30px;
        padding-top: 30px;
        padding-bottom: 10px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const Text = styled.p`
    font-size: 16px;
    line-height: 24px;
    color: #6E7276;
    padding-bottom: 50px;
    margin-bottom: 60px;
    @media (max-width: 1096px){
        font-size: 14px;
        line-height: 20px;
        padding-bottom: 30px;
    }
`

const LocLink = styled(Link)`
    font-size: 16px;
    position: absolute;
    bottom: 0;
`

const Content = (props) => {
    return (
        <article>
            <Container>
                <Grid>
                    {
                        props.ebooks.map(el =>
                            <Item>
                                <NavLink to={'/ebooks/' + el.slug}>
                                    <Img src={el.acf.preview.img} />
                                    <Title>{el.acf.preview.title}</Title>
                                    <Text>{el.acf.preview.text}</Text>
                                </NavLink>
                                <LocLink to={'/ebooks/' + el.slug}>{el.acf.preview.button_text}</LocLink>
                            </Item>
                        )
                    }
                </Grid>
            </Container>
        </article>
    )
}

export default Content