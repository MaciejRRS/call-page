import React, { useState } from 'react'
import { Container } from '../../common/styles'
import styled from 'styled-components'
import ReactHtmlParser from 'react-html-parser'
import SEO from './../../common/SEO/seo'
import ContactForm from '../../common/form/Form'

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    padding-top: 60px;
    @media(max-width: 1198px){
        flex-direction: column-reverse;
        align-items: center;
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    display: ${props => props.mobile ? 'none' : 'block'};
    @media(max-width: 1198px){
        display: ${props => props.mobile ? 'block' : 'none'}; 
    }
    @media(max-width: 1000px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.h2`
    padding: 50px 0 60px;
    font-size: 28px;
    line-height: 40px;
    font-weight: normal;
    color: #6E7276;
    display: ${props => props.mobile ? 'none' : 'block'};
    @media(max-width: 1198px){
        display: ${props => props.mobile ? 'block' : 'none'}; 
    }
    @media(max-width: 1000px){
        font-size: 16px;
        line-height: 22px;
    }
    @media(max-width: 764px){
        padding: 20px 0 30px;
    }
`

const TextPart = styled.div`
    max-width: 953px;
    margin-right: 30px;

    @media(max-width: 1198px){
        margin: 0;
    }
`

const ImgPart = styled.div`
    width: 526px;
    
    img{
        width: 100%;
    }
    
    @media(max-width: 1198px){
        margin-bottom: 60px;
        width: 100%;
        max-width: 1000px;
        text-align: center;

        img{
            width: 526px;
            display: block;
            margin: 0 auto
        }
    }
    @media(max-width: 764px){
        margin-bottom: 30px;
    }
    @media(max-width: 650px){
        img{
            width: 100%;
        }
    }

`

const Content = styled.div`

    h1,h2,h3,h4,h5,h6{

    }
    p{
        padding-bottom: 30px;
        color: #6E7276;
    }
    ul{
        padding-bottom: 30px;
    }
    li{
        padding: 7px 0 8px 25px; 
        position: relative;
        color: #6E7276;

        &::before{
            content: '•';
            position: absolute;
            left: 0;
            color: #377DFF;
            font-weight: 900;
        }
    }

    @media(max-width: 764px){
        h1,h2,h3,h4,h5,h6{
            font-size: 18px;
            line-height: 28px;
        }
        *{
            font-size: 16px;
            line-height: 22px;
        }
    }

    @media(max-width: 540px){
        h1,h2,h3,h4,h5,h6{
            font-size: 18px;
            line-height: 28px;
        }
        *{
            font-size: 14px;
            line-height: 20px;
        }
    }
`

const Link = styled.a`
    width: 248px;
    display: block;
    font-weight: bold;
    border-radius: 6px;
    padding: 15px 0;
    text-align: center;
    background-color: #377DFF;
    color: #ffffff;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    margin-top: 30px;
`

const Img = styled.img`
    display: block;
    margin: 120px auto 0;
    @media(max-width: 1198px){
        width: 100%;
        max-width: 1082px;
        margin-top: 60px;
    }
    @media(max-width: 764px){   
        margin-top: 30px;
    }
`

const Popup = styled.div`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    width: 70vw;
    height: 70vh;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fff;
    z-index: 1000;
    border-radius: 12px;
    box-sizing: border-box;
    padding: 30px;
    overflow-y: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
    @media(max-width: 998px){
        width: 85vw;
        height: 75vh;
    }
`

const PopupBackground = styled.div`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 999;
    background-color: #00000099;
`

const ClosePopup = styled.button`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    position: fixed;
    right: 10vw;
    top: 10vh;
    width: 50px;
    height: 50px;
    border: none;
    background: transparent;
    z-index: 10001;
    cursor: pointer;
    &::before,
    &::after{
        width: 100%;
        height: 3px;
        content: '';
        position: absolute;
        z-index: 10002;
        left: 0;
        top: 0;
        background-color: #fff;
        transform: rotateZ(45deg);
    }

    &::after{
        transform: rotateZ(-45deg);
    }

    @media(max-width: 998px){
        right: 2vw;
        top: 6vh;
        
    }
`

const EbooksItem = (props) => {

    const [isPopupOpened, changePopupStatus] = useState(false)

    return (
        <main>
            <SEO seo={props.post.acf.seo} />
            <PopupBackground onClick={() => { changePopupStatus(false) }} isPopupOpened={isPopupOpened} />
            <ClosePopup onClick={() => { changePopupStatus(false) }} isPopupOpened={isPopupOpened} />
            <Popup isPopupOpened={isPopupOpened}>
                <ContactForm
                    portalId={props.post.acf.content.portal_id ? props.post.acf.content.portal_id : '1206795'}
                    formId={props.post.acf.content.form_id ? props.post.acf.content.form_id : 'ceb9aa10-3684-4819-a78a-2bdbd8e42031'}
                />
            </Popup>
            <Container>
                <Flex>
                    <TextPart>
                        <Title>{props.post.acf.preview.title}</Title>
                        <Text>{props.post.acf.preview.text}</Text>
                        <Content>{ReactHtmlParser(props.post.acf.content.content)}</Content>
                        <Link onClick={() => { changePopupStatus(true) }}>{props.post.acf.content.button_text}</Link>
                    </TextPart>
                    <ImgPart>
                        <Title mobile='true'>{props.post.acf.preview.title}</Title>
                        <Text mobile='true'>{props.post.acf.preview.text}</Text>
                        <img alt={props.post.acf.preview.icon_alt} src={props.post.acf.preview.img} />
                    </ImgPart>
                </Flex>
                <Img alt={props.post.acf.content.icon_alt} src={props.post.acf.content.img} />
            </Container>
        </main>
    )
}

export default EbooksItem