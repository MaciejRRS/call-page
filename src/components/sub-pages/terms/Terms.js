import React from 'react'
import { connect } from "react-redux"
import ReactHtmlParser from 'react-html-parser'
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'

const Terms = (props) => {

    let html = ReactHtmlParser(props.acf.text)
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Content html={html} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[29].acf // Terms
    }
}

export default connect(mapStateToProps, {})(Terms)