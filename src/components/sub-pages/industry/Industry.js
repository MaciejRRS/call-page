import React from 'react'
import { connect } from "react-redux"
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'


const Industry = (props) => {
    return (
        <main>
        <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} />
            <Content acf={props.acf.content} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[6].acf // Industry
    }
}

export default connect(mapStateToProps, {})(Industry)