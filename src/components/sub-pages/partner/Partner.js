import React from 'react'
import { connect } from "react-redux"
import Hero from './components/Hero'
import CreatedFor from './components/CreatedFor'
import Using from './components/Using'
import Numbers from './../../../common/bricks/Numbers'
import Awards from '../../../common/bricks/Awards'
import PartnersType from '../../../common/bricks/PartnersType'
import Steps from './components/Steps'
import Form from './components/Form'
import SEO from '../../../common/SEO/seo'

const Partner = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} />
            <CreatedFor acf={props.acf.created_for} />
            <Using acf={props.acf.using} />
            <Numbers acf={props.acf.numbers} />
            <Awards acf={props.acf.awards} />
            <PartnersType acf={props.acf.partners_type} />
            <Steps marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.steps} />
            <Form acf={props.acf.form} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[9].acf // Partner
    }
}

export default connect(mapStateToProps, {})(Partner)