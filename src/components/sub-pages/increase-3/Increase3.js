import React from 'react'
import { connect } from "react-redux";
import TrustedBy from '../../../common/bricks/TrustedBy'
import Clients from '../../../common/bricks/Clients'
import Generation from '../../../common/bricks/Generation'
import Features from '../../../common/bricks/Features'
import Greater from '../../../common/bricks/Greater'
import Moreandbetter from '../../../common/bricks/Moreandbetter'
import Develope from '../../../common/bricks/Develope'
import VoiceChat from '../../../common/bricks/VoiceChat'
import ProblemsResolving from '../../../common/bricks/ProblemsResolving'
import Dominating from '../../../common/bricks/Dominating'
import Awards from '../../../common/bricks/Awards'
import Questions from '../../../common/bricks/Questions'
import SEO from '../../../common/SEO/seo'

const Increase = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <TrustedBy acf={props.acf.hero} />
            <Clients acf={props.acf.clients} />
            <VoiceChat acf={props.acf.voice_chat} />
            <Greater acf={props.acf.greater} />
            <Generation acf={props.acf.generation} />
            <ProblemsResolving acf={props.acf.problem_resolving} />
            <Moreandbetter acf={props.acf.more_and_better} />
            <Dominating acf={props.acf.dominating_channel} />
            <Features acf={props.acf.features} />
            <Develope marginDesktop='' marginTablet='' marginPhone='' marginBottomDesktop='' marginBottomTablet='120px' marginBottomPhone='80px' acf={props.acf.develope} />
            <Awards acf={props.acf.awards} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[20].acf // 
    }
}

export default connect(mapStateToProps, {})(Increase)