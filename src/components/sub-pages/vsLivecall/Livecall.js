import React from 'react'
import { connect } from "react-redux"
import MoreEffective from '../../../common/bricks/MoreEffective'
import BestAlternative from '../../../common/bricks/BestAlternative'
import TopReasons from '../../../common/bricks/TopReasons'
import ConversationRate from '../../../common/bricks/ConversationRate'
import MoreIntegrations from '../../../common/bricks/MoreIntegrations'
import OtherFeatures from '../../../common/bricks/OtherFeatures'
import Develope from '../../../common/bricks/Develope'
import Awards from '../../../common/bricks/Awards'
import SEO from '../../../common/SEO/seo'

const Livecall = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <MoreEffective acf={props.acf.hero} />
            <BestAlternative acf={props.acf.best_alternative} />
            <TopReasons acf={props.acf.top_reasons} />
            <ConversationRate acf={props.acf.conversation_rate} />
            <MoreIntegrations acf={props.acf.more_integrations} />
            <OtherFeatures acf={props.acf.other_features} />
            <Develope acf={props.acf.develope} />
            <Awards marginDesktop='240px' marginTablet='260px' marginMobile='280px' acf={props.acf.awards} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[13].acf
    }
}

export default connect(mapStateToProps, {})(Livecall)