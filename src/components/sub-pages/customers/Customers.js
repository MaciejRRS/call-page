import React, { useState, useEffect } from 'react'
import { connect } from "react-redux"
import Hero from './components/Hero'
import UseStates from './components/UseStates'
import BenefitsIndustry from './components/BenefitsIndustry'
import Awards from '../../../common/bricks/Awards'
import Numbers from './components/Numbers'
import BlogPart from './components/BlogPart'
import SEO from '../../../common/SEO/seo'

const Customers = (props) => {

    const [productsPrototype, productsPrototypeChange] = useState([...props.caseStudies])
    const [products, productsChange] = useState([...productsPrototype]);

    useEffect(() => {
        productsPrototypeChange([...props.caseStudies])
        productsChange([...props.caseStudies])
    }, [props])

    const FilterByIntegrations = (value) => {
        productsChange(productsPrototype.filter(el => el.acf.type === value))
    }

    const caseStudies = []
    for (let i = 0; i < 4; i++) {
        if (products[i]) {
            caseStudies.push(products[i])
        } else {

        }
    }

    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} />
            <UseStates FilterByIntegrations={FilterByIntegrations} acf={props.acf.examples} />
            <BlogPart caseStudies={caseStudies} />
            <BenefitsIndustry acf={props.acf.benefits_industry} />
            <Awards marginDesktop='180px' marginTablet='120px' marginMobile='90px' acf={props.acf.awards} />
            <Numbers acf={props.acf.numbers} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[3].acf, // Clients
        caseStudies: state.wpAcfReducer.caseStudies
    }
}

export default connect(mapStateToProps, {})(Customers)