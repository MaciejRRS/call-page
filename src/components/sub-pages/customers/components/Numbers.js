import React from 'react'
import { Container, Text } from '../../../../common/styles'
import styled from 'styled-components'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '180px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '120px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: center;
    @media(max-width: 1198px){
    }
    @media(max-width: 764px){
        flex-direction: column;
        align-items: center;
    }
`

const Item = styled.div`
    width: 370px;  
    max-width: 100%; 
    text-align: center;
    padding: 0 15px;

    img{
        max-width: 248px;
    }

    @media(max-width: 1198px){
        img{
            max-width: 180px
        }
    }
    @media(max-width: 764px){
        margin: 30px 0;
    }
`

const Number = styled.h3`
    padding: 40px 0 10px;
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding-top: 25px;
    }
`

const LocText = styled(Text)`
    font-size: 28px;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 22px;
    }
`

const Numbers = (props) => {
    return (
        <Article>
            <Container>
                <Flex>
                    {
                        props.acf.repeater.map(el =>
                            <Item>
                                <img alt={el.img_alt} src={el.img} />
                                <Number>{el.number}</Number>
                                <LocText>{el.text}</LocText>
                            </Item>
                        )
                    }
                </Flex>
            </Container>
        </Article>
    )
}

export default Numbers