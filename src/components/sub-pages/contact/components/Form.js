import React from 'react'
import { Container, Flex } from '../../../../common/styles'
import styled from 'styled-components'
import ContactForm from '../../../../common/form/Form';

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const SubTitle = styled.h2`
    text-align: center;
    color: #6E7276;
    font-size: 28px;
    line-height: 40px;
    margin: 50px 0 120px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        margin: 30px 0 90px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
        margin: 20px 0 40px;
    }
`

const LocFlex = styled(Flex)`
    div:last-child {
        display: none;
    } 
    @media(max-width: 764px){
        flex-direction: column;
        div:nth-child(even){
            display: none;
        }
    }
`

const Param = styled.div`
    color: #6E7276;
    font-size: 22px;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 28px;
    }
`

const LocContainer = styled(Container)`
    max-width: 1000px;
`

const Form = (props) => {

    return (
        <article>
            <LocContainer>
                <Title>{props.acf.title}</Title>
                <SubTitle>{props.acf.sub_title}</SubTitle>
                <ContactForm portalId={props.acf.portal_id} formId={props.acf.form_id} />
                <LocFlex>
                    {
                        props.acf.params.map(el =>
                            <>
                                <Param>{el.text}</Param>
                                <Param>|</Param>
                            </>
                        )
                    }
                </LocFlex>
            </LocContainer>
        </article>
    )
}

export default Form