import React from 'react'
import { connect } from 'react-redux'
import Form from './components/Form'
import SEO from '../../../common/SEO/seo'

const Contact = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Form acf={props.acf} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[8].acf
    }
}

export default connect(mapStateToProps, {})(Contact)