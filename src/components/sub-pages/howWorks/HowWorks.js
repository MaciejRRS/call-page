import React from 'react'
import { connect } from "react-redux"
import Hero from './components/Hero'
import Steps from './components/Steps'
import Features from './components/Features'
import Callback from './components/Callback'
import System from './components/System'
import Repeater from './components/Repeater'
import Video from '../../../common/bricks/Video'
import SEO from '../../../common/SEO/seo'

const HowWorks = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Hero acf={props.acf.hero} anchor={props.acf.navigation.repeater} />
            <Video acf={props.acf.hero} />
            <Steps acf={props.acf.steps} />
            <Features acf={props.acf.features} />
            <Callback acf={props.acf.callback} />
            <System acf={props.acf.system} />
            <Repeater acf={props.acf.Repeater} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[15].acf // How It Works
    }
}

export default connect(mapStateToProps, {})(HowWorks)