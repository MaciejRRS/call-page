import React from 'react'
import styled from 'styled-components'
import { Container } from '../../../../common/styles'
import MiniForm from './../../../../common/mini-form/Form'


const Article = styled.article`
    padding-top: 120px;
    
    @media(max-width: 1198px){
        padding-top: 180px;
    }
    @media(max-width: 764px){
        padding-top: 120px;
    }
`
const List = styled.ul`
    position: relative;
    &::after {
        content: '';
        width: 2px;
        height: 100%;
        position: absolute;
        top: 0;
        left: 50%;
        background: #A7C6FF;

        @media(max-width: 764px) {
            display: none;
        }
    }
    li:nth-child(odd){
        flex-direction: row;

        @media(max-width: 764px) {
            flex-direction: column;
        }
    }
`

const ListItem = styled.li`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row-reverse;
    margin-top: 150px;
    position: relative;


    :first-child {
       margin-top: 0;
    }


    @media(max-width: 764px) {
            flex-direction: column;
            margin-top: 0;
        }

    :before {
    content:'';
    width: 30px;
    height: 30px;
    position: absolute;
    top: 110px;
    left: calc(50% - 15px);
    background: #A7C6FF;
    border-radius: 50%;

    @media(max-width: 764px) {
        display: none;
    }
}

    :first-child {
        :before {
        content:'';
        top: 0;

}
    }

    :last-child {
       padding-bottom: 60px;
       @media(max-width: 764px) {
        padding-bottom: 0;
       }
    }
`

const Img = styled.img`
    max-width: 665px;
    margin-top: 110px;
    @media(max-width: 1600px) {
        max-width: 40vw;
    }
    @media(max-width: 1250px) {
        max-width: 35vw;
    }
    @media(max-width: 764px) {
        max-width: 80vw;
        margin-top: 30px;
    }
`

const TextWrapper = styled.div`
    width: 665px;
    margin-top: 55px;
    @media(max-width: 1600px) {
        max-width: 40vw;
    }

    @media(max-width: 1250px) {
        max-width: 35vw;
    }
    @media(max-width: 764px) {
        max-width: 80vw;
    }
`

const ListTitle = styled.h3`
    font-size: 18px;
    font-weight: bold;
    color: #377DFF;

    @media(max-width: 764px) {
        text-align: center;
        line-height: 28px;
    }
`

const ListSubTitle = styled.h4`
    padding: 30px 0 15px;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    @media(max-width: 1250px) {
        font-size:22px;
        line-height: 30px;
    }

    @media(max-width: 764px) {
        padding: 45px 0 30px;
        text-align: center;
    }
`

const AreaMiniForm = styled.div`
margin-top: 70px;

@media(max-width: 1198px) {
        margin-top: 65px;
    }

    @media(max-width: 764px) {
        margin-top: 45px;
    }
`





const Steps = (props) => {
    return (
        <Article id={props.acf.id} marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <List>
                    {
                        props.acf.repeater.map(el =>
                            <ListItem>
                                <div>
                                    <Img alt={el.img_alt} src={el.img} />
                                </div>
                                <TextWrapper>
                                    <ListTitle>{el.title}</ListTitle>
                                    <ListSubTitle>{el.text}</ListSubTitle>
                                </TextWrapper>
                            </ListItem>
                        )
                    }
                </List>
                <AreaMiniForm>
                    <MiniForm acf={props.acf.form} />
                </AreaMiniForm>

            </Container>
        </Article>
    )
}

export default Steps