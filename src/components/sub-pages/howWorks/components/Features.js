import React from 'react'
import styled from 'styled-components'
import { Container } from '../../../../common/styles'
import Background from './../../../../sprites/bgFeatures.png'


const Article = styled.article`
    padding-top: 180px;
    margin-top: -60px;
    background-color: #fff;

    @media(max-width: 1198px) {
        padding-top: 120px;
    }

    @media(max-width: 764px) {
        padding-top: 180px;
    }
`
const Title = styled.h2`
    text-align: center;
    font-size: clamp(28px, 3.5vw, 45px);
    line-height: clamp(40px, 3.5vw, 60px);
    font-weight: bold;
    margin: 0 0 60px;

    @media(max-width: 1199px) {
        margin: 0 0 25px;
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    font-size: 28px;
    text-align: center;
    line-height: 40px;
    color: #6E7276;
    @media(max-width: 1199px) {
    font-size: 22px;
    line-height: 30px;
}
`

const BgWrapper = styled.div`
    position: relative;
    z-index: 10;  
`

const List = styled.ul`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
`

const ListItem = styled.li`
    font-size: 28px;
    text-align: center;
    color: #377DFF;
    text-transform: uppercase;
    line-height: 28px;
    width: 100%;
    max-width: 25%;
    cursor: pointer;

    &:hover{
        h3{
            color: #377DFF;
        }
    }

    @media(max-width: 1199px) {
    max-width: 50%;
    }

    @media(max-width: 767px) {
    max-width: 80vw;
    margin: 0 auto;
    }
`

const Img = styled.img`
    max-width: 100%;
    width: 100%;
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    transform: translate(0,-50%);
    background-repeat: no-repeat;
    background-size: contain;
    width: 100%;
    max-width: 100%;
    height: 350px;
    top: calc(50% + 150px);
    left: 0;
    z-index: 0;
    @media(max-width: 1199px) {
        display: none;
    }
`



const ListTitle = styled.h3`
    padding: 60px 0;
    font-weight: bold;
    font-size: 18px;
    color: #000000;
    transition: .2s linear;

    @media(max-width: 1199px) {
        padding: 40px 0 30px;
    }
`



const Features = (props) => {
    return (
        <Article id={props.acf.id}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Text>{props.acf.text}</Text>
            </Container>
            <BgWrapper>
                <Container>
                    <List>
                        {
                            props.acf.repeater.map(el =>
                                <ListItem>
                                    <a href={'#' + el.anchor_url}>
                                        <ListTitle>{el.title}</ListTitle>
                                        <Img alt={el.img_alt} src={el.img} />
                                    </a>
                                </ListItem>
                            )
                        }

                    </List>
                </Container>
                <BottomBG></BottomBG>

            </BgWrapper>
        </Article>
    )
}

export default Features