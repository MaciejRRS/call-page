import React from 'react'
import { connect } from "react-redux"
import Develope from '../../../common/bricks/Develope'
import Clients from '../../../common/bricks/Clients'
import Features from '../../../common/bricks/Features'
import Awards from '../../../common/bricks/Awards'
import Generation from '../../../common/bricks/Generation'
import TrustedBy from '../../../common/bricks/TrustedBy'
import ProblemsResolving from '../../../common/bricks/ProblemsResolving'
import Questions from '../../../common/bricks/Questions'
import PartnersType from '../../../common/bricks/PartnersType'
import SEO from '../../../common/SEO/seo'

const CallCenter = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <TrustedBy acf={props.acf.hero} />
            <PartnersType acf={props.acf.partners_type} />
            <ProblemsResolving acf={props.acf.problem_resolving} />
            <Generation acf={props.acf.generation} />
            <Clients acf={props.acf.clients} />
            <Features acf={props.acf.features} />
            <Develope acf={props.acf.develope} />
            <Awards marginDesktop='240px' marginTablet='260px' marginPhone='280px' acf={props.acf.awards} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[11].acf // vsCallCenter
    }
}

export default connect(mapStateToProps, {})(CallCenter)