import React from 'react'
import { connect } from "react-redux"
import MoreEffective from '../../../common/bricks/MoreEffective'
import ReceiveCalls from './components/ReceiveCalls'
import TopReasons from '../../../common/bricks/TopReasons'
import ConversationRate from '../../../common/bricks/ConversationRate'
import OtherFeatures from '../../../common/bricks/OtherFeatures'
import Develope from '../../../common/bricks/Develope'
import SEO from '../../../common/SEO/seo'


const Novocall = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <MoreEffective marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.hero} />
            <TopReasons marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.top_reasons} />
            <ReceiveCalls marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.receive_calls} />
            <ConversationRate marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.conversation_rate} />
            <OtherFeatures marginDesktop='' marginTablet='' marginMobile='' acf={props.acf.other_features} />
            <Develope marginDesktop='' marginTablet='' marginPhone='' marginBottomDesktop='' marginBottomTablet='280px' marginBottomPhone='220px' acf={props.acf.develope_kopia} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[14].acf // vsNovocall
    }
}

export default connect(mapStateToProps, {})(Novocall)