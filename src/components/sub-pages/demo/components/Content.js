import React from 'react'
import styled from 'styled-components'
import { Container } from '../../../../common/styles'
import HubspotForm from 'react-hubspot-form'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '90px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    padding-bottom: 120px;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        padding-bottom: 90px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding-bottom: 60px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 5fr 7fr;
    grid-column-gap: 30px;
    @media(max-width: 1198px){
        display: flex;
        flex-direction: column;
    }
`

const Aside = styled.aside`
    width: 100%;
    @media(max-width: 1198px){
        width: auto;
        margin: 0 90px 70px;
    }
    @media(max-width: 764px){
        margin: 0 0 45px;
    }
`

const TextTitle = styled.h2`
    font-size: 28px;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const List = styled.ul`
    padding: 60px 0;
    display: grid;
    grid-row-gap: 30px;

    @media(max-width: 1198px){
        padding: 45px 0;
    }
    @media(max-width: 764px){
        padding: 30px 0;
    }

    li{
        position: relative;
        padding-left: 20px;
        color: #6E7276;
        font-size: 20px;
        line-height: 24px;

        @media(max-width: 1198px){
            font-size: 16px;
            line-height: 20px;
        }

        &::before{
            content: '•';
            position: absolute;
            left: 0;
            color: #377DFF;
        }
    }
`

const Text = styled.p`
    color: #6E7276;
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
    }
`

const Section = styled.section`
    box-sizing: border-box;
    padding: 30px;
    margin: 0 30px;
    box-shadow: 0 3px 6px 0 #00000016;
    width: 100%;
    @media(max-width: 1198px){
        margin: 0;
    }
    @media(max-width: 764px){
        padding: 15px;
    }
`

const Content = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.hero.title}</Title>
                <Grid>
                    <Aside>
                        <TextTitle>{props.acf.text_part.title}</TextTitle>
                        <List>
                            {props.acf.text_part.list.map(el =>
                                <li>{el.text}</li>
                            )}
                        </List>
                        <Text>{props.acf.text_part.text}</Text>
                    </Aside>
                    <Section>
                        <HubspotForm
                            portalId={props.acf.form.portal_id}
                            formId={props.acf.form.form_id}
                        />
                    </Section>
                </Grid>
            </Container>
        </Article>
    )
}

export default Content