import React from 'react'
import { connect } from "react-redux";
import TrustedBy from '../../../common/bricks/TrustedBy'
import Clients from '../../../common/bricks/Clients'
import Generation from '../../../common/bricks/Generation'
import ProblemsResolving from '../../../common/bricks/ProblemsResolving'
import Greater from '../../../common/bricks/Greater'
import Moreandbetter from '../../../common/bricks/Moreandbetter'
import Features from '../../../common/bricks/Features'
import Awards from '../../../common/bricks/Awards'
import Develope from '../../../common/bricks/Develope'
import Questions from '../../../common/bricks/Questions'
import SEO from '../../../common/SEO/seo'

const Increase1 = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <TrustedBy acf={props.acf.hero} />
            <Clients acf={props.acf.clients} />
            <Greater acf={props.acf.greater} />
            <Generation acf={props.acf.generation} />
            <ProblemsResolving acf={props.acf.problem_resolving} />
            <Moreandbetter acf={props.acf.more_and_better} />
            <Features acf={props.acf.features} />
            <Awards acf={props.acf.awards} />
            <Develope marginDesktop='' marginTablet='' marginPhone='' marginBottomDesktop='' marginBottomTablet='120px' marginBottomPhone='80px' acf={props.acf.develope} />
            <Questions acf={props.acf.questions} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[18].acf
    }
}

export default connect(mapStateToProps, {})(Increase1)