import React from 'react'
import { connect } from "react-redux";
import Steps from './components/Steps'
import SEO from '../../../common/SEO/seo'


const ApiDevelopers = (props) => {
    return (
        <main>
            <SEO seo={props.acf.seo} />
            <Steps acf={props.acf.content} />
        </main>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[21].acf // 
    }
}

export default connect(mapStateToProps, {})(ApiDevelopers)