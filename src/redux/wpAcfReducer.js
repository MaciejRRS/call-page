import axios from 'axios';

let initialState = {
    totalPosts: null,
    totalEbooks: null,
    totalCaseStudies: null,
    blogPosts: [],
    pages: [],
    ebooks: [],
    caseStudies: [],
    concreateIndustry: [],
    isFetchingAcf: true,
    isFetchingAddData: true,
    currentBlogPage: 1,
    isAllPosts: false,
    blogWidgetAcf: false,
    concreteBlogPost: false
}

const INIT = "INIT"
const BLOG_PAGINATION = "BLOG_PAGINATION"
const BLOG_INIT = "BLOG_INIT"
const SECONDARY_INIT = "SECONDARY_INIT"
const CONCRETE_POST = "CONCRETE_POST"

const wpVariablesReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT: {
            return {
                ...state,
                isFetchingAcf: false,
                pages: action.pages.data.reverse()
            }
        }
        case SECONDARY_INIT: {
            if (state.concreteBlogPost) {
                action.posts.data.push(state.concreteBlogPost)
            }
            return {
                ...state,
                isFetchingAddData: false,
                blogPosts: action.posts.data,
                ebooks: action.ebooks.data.reverse(),
                caseStudies: action.caseStudies.data,
                concreateIndustry: action.concreateIndustry.data,
                totalPosts: action.posts.headers['x-wp-total'],
                totalEbooks: action.ebooks.headers['x-wp-total'],
                totalCaseStudies: action.caseStudies.headers['x-wp-total'],
            }
        }
        case BLOG_PAGINATION: {
            let nextState = { ...state }
            nextState.currentBlogPage = action.page
            return nextState
        }
        case BLOG_INIT: {
            let nextState = { ...state }
            nextState.blogPosts = action.pages.data
            nextState.isAllPosts = true
            return nextState
        }
        case CONCRETE_POST: {
            return {
                ...state,
                blogPosts: action.blogPost.data,
                blogWidgetAcf: action.blogWidgetAcf.data,
                concreteBlogPost: action.blogPost.data[0]
            }
        }
        default: {
            return state
        }
    }
}

export default wpVariablesReducer

const initialize = (pages) => ({ type: INIT, pages })
const blogPagination = (page) => ({ type: BLOG_PAGINATION, page })
const blogInit = (pages) => ({ type: BLOG_INIT, pages })
const initializeSecondary = (posts, ebooks, caseStudies, concreateIndustry) => ({ type: SECONDARY_INIT, posts, ebooks, caseStudies, concreateIndustry })
const concretePost = (blogPost, blogWidgetAcf) => ({ type: CONCRETE_POST, blogPost, blogWidgetAcf })

export const initializeThunk = () => {

    let one = 'https://callpage-en.redrocks.dev/wp-json/acf/v3/pages?per_page=100'

    const requestOne = axios.get(one)

    return (dispatch) => {
        axios.all([requestOne]).then(axios.spread((...responses) => {
            const responseOne = responses[0]

            dispatch(initialize(responseOne))
        }))

    }
}

export const secondaryInitializeThunk = () => {
    let two = 'https://callpage-en.redrocks.dev/wp-json/wp/v2/posts'
    let three = 'https://callpage-en.redrocks.dev/wp-json/wp/v2/ebooks?per_page=100'
    let four = 'https://callpage-en.redrocks.dev/wp-json/wp/v2/casestudies?per_page=100'
    let five = 'https://callpage-en.redrocks.dev/wp-json/wp/v2/concreate_industry'

    const requestTwo = axios.get(two)
    const requestThree = axios.get(three)
    const requestFour = axios.get(four)
    const requestFive = axios.get(five)

    return (dispatch) => {
        axios.all([requestTwo, requestThree, requestFour, requestFive]).then(axios.spread((...responses) => {
            const responseTwo = responses[0]
            const responseThree = responses[1]
            const responseFour = responses[2]
            const responseFive = responses[3]

            dispatch(initializeSecondary(responseTwo, responseThree, responseFour, responseFive))
        }))

    }
}

export const blogInitThunk = () => {
    return (dispatch) => {
        axios.get('https://callpage-en.redrocks.dev/wp-json/wp/v2/posts?per_page=1000')
            .then((res) => {
                dispatch(blogInit(res))
            })
    }
}

export const blogPaginationThunk = (page) => {
    return (dispatch) => {
        dispatch(blogPagination(page))
    }
}

export const concretePostThunk = (slug) => {

    let one = `https://callpage-en.redrocks.dev/wp-json/wp/v2/posts?slug=${slug}`
    let two = 'https://callpage-en.redrocks.dev/wp-json/acf/v3/options/options'

    const requestOne = axios.get(one)
    const requestTwo = axios.get(two)

    return (dispatch) => {
        axios.all([requestOne, requestTwo]).then(axios.spread((...responses) => {
            const responseOne = responses[0]
            const responseTwo = responses[1]

            dispatch(concretePost(responseOne, responseTwo))
        }))

    }
}

