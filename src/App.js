import React from 'react'
import { connect } from "react-redux"
import { initializeThunk, secondaryInitializeThunk, blogInitThunk, concretePostThunk } from "./redux/wpAcfReducer"
import { Route, Switch, } from "react-router-dom"
import { useEffect } from "react"
import { useLocation } from "react-router-dom"
import MainPage from './components/main-page/MainPage'
import Header from './components/header/Header'
import Footer from './components/footer/Footer'
import Customers from './components/sub-pages/customers/Customers'
import Features from './components/sub-pages/features/Features'
import Integrations from './components/sub-pages/integrations/Integrations'
import Industry from './components/sub-pages/industry/Industry'
import Privacy from './components/sub-pages/privacy/Privacy'
import Sequrity from './components/sub-pages/sequrity/Sequrity'
import Terms from './components/sub-pages/terms/Terms'
import Contact from './components/sub-pages/contact/Contact'
import Partner from './components/sub-pages/partner/Partner'
import HowWorks from './components/sub-pages/howWorks/HowWorks'
import Livechats from './components/sub-pages/vsLivechats/Livechats'
import CallCenter from './components/sub-pages/vsCallcenter/CallCenter'
import Popups from './components/sub-pages/vsPopups/Popups'
import LiveCall from './components/sub-pages/vsLivecall/Livecall'
import NovoCall from './components/sub-pages/vsNovocall/Novocall'
import Ebooks from './components/ebooks/Ebooks'
import EbooksItem from './components/ebooks/EbooksItem'
import Blog from './components/blog/Blog'
import BlogItem from './components/blog/BlogItem'
import ApiDevelopers from './components/sub-pages/api-developers/ApiDevelopers'
import ConcreteIndustry from './components/sub-pages/concreate-industry/ConcreateIndustry'
import CaseStudies from './components/case-studies/CaseStudies'
import CaseStudiesItem from './components/case-studies/CaseStudiesItem'
import Increase1 from './components/sub-pages/increase-1/Increase1'
import Increase2 from './components/sub-pages/increase-2/Increase2'
import Increase3 from './components/sub-pages/increase-3/Increase3'
import About from './components/sub-pages/about/About'
import Error from './components/sub-pages/error/Error'
import CountryCovering from './components/sub-pages/country-covering/CountryCovering'
import Careers from './components/sub-pages/careers/Careers'
import Demo from './components/sub-pages/demo/Demo'
import Pricing from './components/sub-pages/pricing/Pricing'
import Loader from './common/loader/Loader'

/* eslint-disable */

function App(props) {

  const ScrollToTop = () => {
    const location = useLocation()

    useEffect(() => {
      if (location.pathname != '/blog') {
        window.scrollTo(0, 0)
      }
    }, [location.pathname])

    return null
  }

  useEffect(() => {
    props.initializeThunk()
    if (location.pathname.includes('/blog/')) {
      props.concretePostThunk(location.pathname.substr(6))
    }
  }, [])

  useEffect(() => {
    if (!props.isFetchingAcf && props.isFetchingAddData) {
      props.secondaryInitializeThunk()
      props.blogInitThunk()
    }
  }, [props])

  return (
    <>
      {
        props.isFetchingAcf
          ? null
          : <>
            <ScrollToTop />
            <Header />

            <Switch>
              <Route exact path="/" render={() => <MainPage />} />
              <Route exact path="/blog" render={() => <Blog />} />
              <Route exact path="/ebooks" render={() => <Ebooks />} />
              <Route exact path="/casestudies" render={() => <CaseStudies />} />
              <Route exact path="/customers" render={() => <Customers />} />
              <Route exact path="/integrations" render={() => <Integrations />} />
              <Route exact path="/partners" render={() => <Partner />} />
              <Route exact path="/how-it-works" render={() => <HowWorks />} />
              <Route exact path="/developers" render={() => <ApiDevelopers />} />
              <Route exact path="/about" render={() => <About />} />
              <Route exact path="/careers" render={() => <Careers />} />
              <Route exact path="/demo" render={() => <Demo />} />
              <Route exact path="/pricing" render={() => <Pricing />} />
              <Route exact path="/contact" render={() => <Contact />} />
              <Route exact path="/all-features" render={() => <Features />} />

              <Route exact path="/privacy-policy" render={() => <Privacy />} />
              <Route exact path="/callpage-security-and-data-storage" render={() => <Sequrity />} />
              <Route exact path="/terms-of-service" render={() => <Terms />} />

              <Route exact path="/livechats-alternative" render={() => <Livechats />} />
              <Route exact path="/callcenter-alternative" render={() => <CallCenter />} />
              <Route exact path="/popups-alternative" render={() => <Popups />} />
              <Route exact path="/livecall-alternative" render={() => <LiveCall />} />
              <Route exact path="/novocall-alternative" render={() => <NovoCall />} />

              <Route exact path="/country-covering" render={() => <CountryCovering />} /> {/* change url */}
              <Route exact path="/time-to-lead" render={() => <Increase1 />} />
              <Route exact path="/lead-generation" render={() => <Increase2 />} />
              <Route exact path="/improve-ux" render={() => <Increase3 />} />
              <Route exact path="/industries" render={() => <Industry />} />

              <Route exact path="/industry" render={() => <Industry />} />

              <Route exact path="/customers" render={() => <Customers />} />

              {
                props.blogPosts.map(el =>
                  <Route exact path={'/blog/' + el.slug} render={() => <BlogItem post={el} blogPages={props.blogPosts} isFetchingAddData={props.isFetchingAddData} blogWidgetAcf={props.blogWidgetAcf.acf.block_kopia} />} />
                )
              }
              {
                props.ebooks.map(el =>
                  <Route exact path={'/ebooks/' + el.slug} render={() => <EbooksItem post={el} />} />
                )
              }
              {
                props.caseStudies.map(el =>
                  <Route exact path={'/casestudies/' + el.slug} render={() => <CaseStudiesItem post={el} />} />
                )
              }

              {
                props.concreateIndustry.map(el =>
                  <Route exact path={'/concrete-industry/' + el.slug} render={() => <ConcreteIndustry acf={el.acf} />} />
                )
              }

              {props.isFetchingAddData
                ? <Route component={Loader} />
                : location.pathname.includes('/blog')
                  ? props.isAllPosts
                    ? <Route component={Error} />
                    : <Route component={Loader} />
                  : <Route component={Error} />
              }

            </Switch>

            <Footer withForm={true} />
          </>
      }
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    isFetchingAcf: state.wpAcfReducer.isFetchingAcf,
    blogPosts: state.wpAcfReducer.blogPosts,
    totalPosts: state.wpAcfReducer.totalPosts,
    ebooks: state.wpAcfReducer.ebooks,
    caseStudies: state.wpAcfReducer.caseStudies,
    concreateIndustry: state.wpAcfReducer.concreateIndustry,
    blogWidgetAcf: state.wpAcfReducer.blogWidgetAcf,
    isFetchingAddData: state.wpAcfReducer.isFetchingAddData,
    isAllPosts: state.wpAcfReducer.isAllPosts
  }
}

export default connect(mapStateToProps, { initializeThunk, secondaryInitializeThunk, blogInitThunk, concretePostThunk })(App)
